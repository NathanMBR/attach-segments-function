# Uso

A função "attachSegments" recebe dois parâmetros: "products", que recebe todo o payload de `/api/products/all`, e "segments", que recebe apenas o valor de `.segmentos` do payload `/api/tabs`. A função retorna o payload de produtos com os segmentos atualizados.