const attachSegments = (products, segments) => {
    products.forEach(product => {
        if (
            product &&
            product.resumo &&
            product.resumo.principal &&
            product.resumo.principal.organizacionais
        ) {
            const segmentCode = product.resumo.principal.organizacionais.segmento;
            segments.forEach(segment => {
                if (segment.cod === segmentCode)
                    product.resumo.principal.organizacionais.segmento = segment;
            })
        }
    });

    return products;
}