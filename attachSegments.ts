interface Produto {
    readonly resumo: {
        readonly principal: {
            readonly organizacionais: {
                readonly segmento: number
            }
        }
    }
}

const getEditableObject = (object: Readonly<Record<string, any>>): Record<string, any> => {
    const keys = Object.keys(object);
    let editableObject: Record<string, any> = {}; 

    keys.forEach(key => {
        const property = object[key];

        if (typeof property === "object" && !Array.isArray(property)) {
            editableObject[key] = getEditableObject(property);
        } else {
            editableObject[key] = property;
        }
    });

    return editableObject;
};

const attachSegmentsTypescript = (rawProducts: Array<Produto>, segments: Array<Record<string, any>>) => {
    const products = rawProducts.map(getEditableObject);

    products.forEach(product => {
        if (
            product &&
            product.resumo &&
            product.resumo.principal &&
            product.resumo.principal.organizacionais
        ) {
            const segmentCode = product.resumo.principal.organizacionais.segmento;
            segments.forEach(segment => {
                if (segment.cod === segmentCode)
                    product.resumo.principal.organizacionais.segmento = segment;
            })

            console.log(product.resumo.principal.organizacionais.segmento);
        }
    });

    return products;
}

const getRawProduct = (value: number): Produto => {
    const product = {
        resumo: {
            principal: {
                organizacionais: {
                    segmento: value
                }
            }
        }
    } as const;

    return product;
}

const rawArr: Array<Produto> = [getRawProduct(4010), getRawProduct(4020), getRawProduct(4030)];
attachSegmentsTypescript(rawArr, [{cod: 4010, desc: "4010" }, {cod: 4030, desc: "4030"}]);